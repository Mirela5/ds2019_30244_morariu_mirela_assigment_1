import {EventEmitter} from "events";
import RestClient from "../rest/RestClient";



const client= new RestClient("doctor","password");


class Model extends EventEmitter{
    constructor(){
        super();
        this.state={
            caregivers: [{
                address:"address1",
                birth_date:"bithdate1",
                email: "mirela@yahoo.com",
                gender:"F",
                name: "name1",
                password: "pass1",
                username: "username1"
              
            },{
                address:"address1",
                birth_date:"bithdate1",
                email: "mirela@yahoo.com",
                gender:"F",
                name: "name1",
                password: "pass1",
                username: "username1"
              
            }],
            newCaregiver:{
                address:" ",
                birth_date:" ",
                email: " ",
                gender:" ",
                name: " ",
                password: " ",
                username: " "
              
            },
            selectedCaregiverIndex: -1

        };
    }
    changeSelectedCaregiverIndex(index) {
        this.state = {
            ...this.state,
            selectedCaregiverIndex: index
        };
        this.emit("change", this.state);
    }

    

changeNewCaregiverProperty(property,value) {
    this.state = {
        ...this.state,
        newCaregiver: {
            ...this.state.newCaregiver,
            [property]: value
        }
    };
    this.emit("change", this.state);
}


newCaregiverList(caregivers){
    this.state = {
        ...this.state,
        caregivers:caregivers
    };
    this.emit("change", this.state);

}



loadCaregiver(){
    return client.loadAllCaregivers().then(caregivers => {
        this.state = { 
            ...this.state, 
            caregivers: caregivers
        };
        this.emit("change", this.state);
    })
}

addCaregiver(address,birth_date,email,gender,name,password,username){
    return client.createCaregiver(address,birth_date,email,gender,name,password,username)
    .then(caregiver=> this.appendCaregiver(caregiver));

}

appendCaregiver(caregiver){
    this.state = {
        ...this.state,
        caregivers: this.state.caregivers.concat([caregiver])
    };
   
    this.emit("change", this.state);

}

/*deleteCaregiver(index){
    var id=this.getCaregiverId(index);
    return client.deleteCaregiver(id);
}

getCaregiverId(index){
    return this.state.caregivers[index].id;

}*/
     

}


const modelCaregiver = new Model();



export default modelCaregiver;