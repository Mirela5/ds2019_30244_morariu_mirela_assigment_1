import React from "react";
import './PatientOps.css'; 
import './LoginUser.css';

const MedicationList = ({ medications }) => (
    
    <div>
        <div className="jumbotron3">  
   
        <div className="container2"> 
         <h1 >All the medication</h1>

       
         <table className="table table-bordered">
         <thead>
            <tr className="table-primary">
                    <th>Name</th>
                    <th>Side effects</th>
    
                 
                </tr>
            </thead>
            <tbody>
                {
                    medications.map(( medication, index) => (
                        <tr className="table-light" key={index}>
                            <td>{medication.name}</td>
                            <td>{medication.sideEffects}</td>

                            
 
                          
                        </tr>
                    ))
                }
            </tbody>
        </table>
        <br />
       
    </div>
    </div>
    </div>
    
);

export default MedicationList;