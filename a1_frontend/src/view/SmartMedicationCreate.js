import React, { Component } from "react";
import modelMedication from "../model/modelMedication";
import MedicationCreate from "./MedicationCreate";
import operationsPresenter from "../presenter/operationsPresenter";

const mapModelStateToComponentState = modelState => ({
  
    name: modelState.newMedication.name,
    sideEffects: modelState.newMedication.sideEffects
    
});

export default class SmartMedicationCreate extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState( modelMedication.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        modelMedication.addListener("change", this.listener);
    }

    componentWillUnmount() {
        modelMedication.removeListener("change", this.listener);
    }

    render() {
        return (
            <MedicationCreate
                onAddMedication={operationsPresenter.onAddMedication}
                onChangeMedication={operationsPresenter.onChangeMedication}
                
                name={this.state.name}
                sideEffects={this.state.sideEffects}
             
                > </MedicationCreate>
        );
    }
}