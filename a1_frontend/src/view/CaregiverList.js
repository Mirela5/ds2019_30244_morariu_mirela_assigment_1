import React from "react";
import './PatientOps.css'; 
import './LoginUser.css';

const CaregiverList = ({ caregivers }) => (
    
    <div>
        <div className="jumbotron3">  
   
        <div className="container2"> 
         <h1 >All the caregivers</h1>


         <table className="table table-bordered">
             <thead>
                <tr className="table-primary">
                        <th>Address</th>
                        <th>Birth Date</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Name</th>
                        <th>Password</th>
                        <th>Username</th>
                </tr>
            </thead>

            <tbody>
                {
                    caregivers.map((caregiver, index) => (
                        <tr className="table-light" key={index}>
                            <td>{caregiver.address}</td>
                            <td>{caregiver.birth_date}</td>
                            <td>{caregiver.email}</td>
                            <td>{caregiver.gender}</td>
                            <td>{caregiver.name}</td>
                            <td>{caregiver.password}</td>
                            <td>{caregiver.username}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
        <br />
       
        </div>
        </div>
    </div>
    
);

export default CaregiverList;