import React from "react";
import 'bootstrap/dist/css/bootstrap.css';

const MedicationCreate = ({ name, sideEffects, onAddMedication, onChangeMedication }) => (
    <div>
         <div className="jumbotron3">  
        
        <div className="container2"> 
        <h1 >Add Medication</h1>



        <div className="form-group">
              <label htmlFor="aut">Name:</label>
              <input type="name" className="form-control" id="dat" value={name} 
                onChange={ e => onChangeMedication("name", e.target.value) } />
        </div>

        <div className="form-group">
              <label htmlFor="aut">sideEffects:</label>
              <input type="sideEffects" className="form-control" id="dat" value={sideEffects} 
                onChange={ e => onChangeMedication("sideEffects", e.target.value) } />
        </div>
       

            <button type="button" className="btn btn-primary" onClick={onAddMedication}>Create!</button>
        </div>
    </div>
    </div>
);

export default MedicationCreate;