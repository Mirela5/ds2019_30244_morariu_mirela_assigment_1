import React from "react";
import './PatientOps.css'; 
import './LoginUser.css';

const MedicationDelete = ({ medications, onDeleteMedication }) => (
    
    <div>
        <div className="jumbotron3">  
   
        <div className="container "> 
         <h1 >Medication</h1>

       
         <table className="table table-bordered">
         <thead>
            <tr className="table-primary">
                    <th>Name</th>
                    <th>Side effects</th>

                    
                </tr>
            </thead>
            <tbody>
                {
                    medications.map((medication, index) => (
                        <tr className="table-light" key={index}>
                            <td>{medication.name}</td>
                            <td>{medication.sideEffects}</td>
                        
                           
                            <td> <button className="button muted-button" onClick={() => onDeleteMedication(index)}>Delete</button> </td>
                         
                          
                        </tr>
                    ))
                }
            </tbody>
        </table>
        <br />
       
    </div>
    </div>
    </div>
    
);

export default MedicationDelete;